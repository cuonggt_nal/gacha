<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patterns', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('quantity');
            $table->string('message_file')->nullable();
            $table->float('probability');
            $table->string('image_file')->nullable();
            $table->string('fbx_file')->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patterns');
	}

}
