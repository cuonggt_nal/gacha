<?php

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::table('users')->truncate();

        User::create (
            array(
                'username' => 'admin',
                'password' => Hash::make('1234'),
            )
        );
    }

}