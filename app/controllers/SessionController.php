<?php

class SessionController extends BaseController {

    public function create() {
        return View::make('session.create');
    }

    public function store() {

        $credentials = [
            'username' => Input::get('username'),
            'password' => Input::get('password')
        ];

        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return Redirect::route('session.create')->withErrors($validator);
        }

        if (Auth::attempt($credentials)) {
            return Redirect::route('home.index');
        }

        return Redirect::route('session.create');
    }

    public function destroy() {
        Auth::logout();

        return Redirect::to('/');
    }
}