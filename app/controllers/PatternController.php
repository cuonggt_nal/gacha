<?php

class PatternController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $patterns = Pattern::paginate(Config::get('app.item_per_page'));
        
		return View::make('pattern.index')
                ->with(array(
                    'patterns' => $patterns,
                ));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('pattern.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required',
            'quantity' => 'required|integer|min:1',
            'message_file' => 'required|image|max:1000',
            'probability' => 'required|numeric|between:0,100',
            'image_file' => 'required|image|max:1000',
            'fbx_file' => 'required|max:1000',
        );
        // validate against the inputs from our form
        $validator = Validator::make(Input::all(), $rules);
        // check if the validator failed
        if ($validator->fails()) {
            // redirect our user back to the form with the errors from the validator
            return Redirect::route('pattern.create')
                ->withErrors($validator)
                ->withInput();
        }

        $pattern = new Pattern();
        $pattern->name = Input::get('name');
        $pattern->quantity = Input::get('quantity');
        $pattern->probability = Input::get('probability');
        if (Input::has('active')) {
            $pattern->active = Input::get('active');
        } else {
            $pattern->active = false;
        }

        if (Input::hasFile('message_file')) {
            $message_file = Input::file('message_file');
            $filename = $this->_generateFileName($message_file);
            $upload = $message_file->move(Config::get('app.pattern_message_file_path'), $filename);
            if ($upload) {
                $pattern->message_file = $filename;
            }
        }
        if (Input::hasFile('image_file')) {
            $image_file = Input::file('image_file');
            $filename = $this->_generateFileName($image_file);
            $upload = $image_file->move(Config::get('app.pattern_image_file_path'), $filename);
            if ($upload) {
                $pattern->image_file = $filename;
            }
        }
        if (Input::hasFile('fbx_file')) {
            $fbx_file = Input::file('fbx_file');
            $filename = $this->_generateFileName($fbx_file);
            $upload = $fbx_file->move(Config::get('app.pattern_fbx_file_path'), $filename);
            if ($upload) {
                $pattern->fbx_file = $filename;
            }
        }
        $pattern->save();

        return Redirect::route('pattern.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $pattern = Pattern::find($id);
        if (!$pattern) {
            return Redirect::route('pattern.index');
        }

		return View::make('pattern.edit')
                ->with(array(
                    'pattern' => $pattern,
                ));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$pattern = Pattern::find($id);
        if (!$pattern) {
            return Redirect::route('pattern.index');
        }

        $rules = array(
            'name' => 'required',
            'quantity' => 'required|integer|min:1',
            'probability' => 'required|numeric|between:0,100',
        );
        if (Input::hasFile('message_file')) {
            $rules['message_file'] = 'image|max:1000';
        }
        if (Input::hasFile('image_file')) {
            $rules['image_file'] = 'image|max:1000';
        }
        if (Input::hasFile('fbx_file')) {
            $rules['fbx_file'] = 'max:1000';
        }
        // validate against the inputs from our form
        $validator = Validator::make(Input::all(), $rules);
        // check if the validator failed
        if ($validator->fails()) {
            // redirect our user back to the form with the errors from the validator
            return Redirect::route('pattern.create')
                ->withErrors($validator)
                ->withInput();
        }

        $pattern->name = Input::get('name');
        $pattern->quantity = Input::get('quantity');
        $pattern->probability = Input::get('probability');
        if (Input::has('active')) {
            $pattern->active = Input::get('active');
        } else {
            $pattern->active = false;
        }
        
        if (Input::hasFile('message_file')) {
            $message_file = Input::file('message_file');
            $filename = $this->_generateFileName($message_file);
            $upload = $message_file->move(Config::get('app.pattern_message_file_path'), $filename);
            if ($upload) {
                File::delete(Config::get('app.pattern_message_file_path') . '/' . $pattern->message_file);
                $pattern->message_file = $filename;
            }
        }
        if (Input::hasFile('image_file')) {
            $image_file = Input::file('image_file');
            $filename = $this->_generateFileName($image_file);
            $upload = $image_file->move(Config::get('app.pattern_image_file_path'), $filename);
            if ($upload) {
                File::delete(Config::get('app.pattern_image_file_path') . '/' . $pattern->image_file);
                $pattern->image_file = $filename;
            }
        }
        if (Input::hasFile('fbx_file')) {
            $fbx_file = Input::file('fbx_file');
            $filename = $this->_generateFileName($fbx_file);
            $upload = $fbx_file->move(Config::get('app.pattern_fbx_file_path'), $filename);
            if ($upload) {
                File::delete(Config::get('app.pattern_fbx_file_path') . '/' . $pattern->fbx_file);
                $pattern->fbx_file = $filename;
            }
        }
        $pattern->save();

        return Redirect::route('pattern.index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$pattern = Pattern::find($id);
        if (!$pattern) {
            return Response::json(array(
                'error' => true,
            ));
        }
        File::delete(Config::get('app.pattern_message_file_path') . '/' . $pattern->message_file);
        File::delete(Config::get('app.pattern_image_file_path') . '/' . $pattern->image_file);
        File::delete(Config::get('app.pattern_fbx_file_path') . '/' . $pattern->fbx_file);
        $pattern->delete();

        return Response::json(array(
            'success' => true,
        ));
	}

    private function _generateFileName($file) {
        $filename = str_random(3) . time() . '.' . $file->getClientOriginalExtension();
        return strtolower($filename);
    }


}
