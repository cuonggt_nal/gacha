@extends('layouts.master')

@section('title')
GACHA | Player Results
@stop
@section('content')
<div class="panel panel-primary">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left">Player Results</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
                <th>#</th>
                <th><input type="checkbox" id="checkall"></th>
                <th>Name</th>
                <th>Age</th>
                <th>Tel</th>
                <th>Pattern</th>
                <th>User_PhoneID</th>
                <th>Datetime</th>
                <th class="text-center">Manager</th>
            </thead>
            <tbody>
                <?php $no = ($results->getCurrentPage() - 1) * $results->getPerPage(); ?>
                @foreach ($results as $result)
                <?php ++$no; ?>
                <tr id="tr_{{ $no }}">
                    <input type="hidden" id="result_id_{{ $no }}" value="{{ $result->id }}" />
                    <td>{{ $no }}</td>
                    <td>
                        <input type="checkbox" id="check_{{ $no }}">
                    </td>
                    <td>{{ $result->player_name }}</td>
                    <td>{{ $result->player_age }}</td>
                    <td>{{ $result->player_tel }}</td>
                    <?php
                    $pattern = $result->pattern()->first();
                    ?>
                    <td>{{ $pattern ? $pattern->name : '' }}</td>
                    <td>{{ $result->player_phoneid }}</td>
                    <td>{{ date('Y/m/d', strtotime($result->created_at)) }}</td>
                    <td class="text-center">
                        <button title="Remove" class="btn btn-danger" id="deleteButton{{ $result->id }}" onclick="removeResultById({{ $result->id }})"><span class="glyphicon glyphicon-remove"></span></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="pull-left"><button class="btn btn-danger" id="remove_selected">Delete Selected</button></div>
        <div class="pull-right">
            {{ $results->links() }}
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeResultById(id) {
        if (confirm('Do you really want to delete?')) {
            var url = '{{ URL::route('result.index') }}/' + id;
            $.ajax({
                url : url,
                type : 'DELETE',
                success : function(data) {
                    window.location.href = '{{ URL::route('result.index') }}';
                }
            });
        }
        return false;
    }
    $(function() {
        $("#checkall").click(function() {
            $("[id^=check_]").prop('checked', $("#checkall").is(':checked'));
        });
        $("#remove_selected").click(function() {
            if (confirm('Do you really want to delete?')) {
                var start = {{ ($results->getCurrentPage() - 1) * $results->getPerPage() + 1 }};
                var end = {{ $no }};
                for (var i = start; i <= end; i++) {
                    var checked = $("#check_" + i).is(':checked');
                    if (checked) {
                        var url = '{{ URL::route('result.index') }}/' + $("#result_id_" + i).val();
                        $.ajax({
                            url : url,
                            type : 'DELETE',
                            success : function(data) {
                                $("#tr_" + i).remove();
                            }
                        });
                    }
                }
                window.location.href = '{{ URL::route('result.index') }}';
            }
            return false;
        });
    });
</script>
@stop