@extends('layouts.master')

@section('title')
GACHA | List of Patterns
@stop
@section('content')
<div class="panel panel-primary">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left" style="padding-top: 7.5px;">List of Patterns</h3>
        <div class="btn-group pull-right">
            <a class="btn btn-success" href="{{ URL::route('pattern.create') }}">Add Pattern</a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
                <th>#</th>
                <th><input type="checkbox" id="checkall"></th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Message</th>
                <th>Probability</th>
                <th>Image File</th>
                <th>3D File (FBX)</th>
                <th>&nbsp;</th>
                <th class="text-center">Manager</th>
            </thead>
            <tbody>
                <?php $no = ($patterns->getCurrentPage() - 1) * $patterns->getPerPage(); ?>
                @foreach ($patterns as $pattern)
                <?php ++$no; ?>
                <tr id="tr_{{ $no }}">
                    <input type="hidden" id="pattern_id_{{ $no }}" value="{{ $pattern->id }}" />
                    <td>{{ $no }}</td>
                    <td>
                        <input type="checkbox" id="check_{{ $no }}">
                    </td>
                    <td>{{ $pattern->name }}</td>
                    <td>{{ $pattern->quantity }}</td>
                    <td><img src="{{ asset('pattern_message_files/' . $pattern->message_file) }}" width="80" class="img-thumbnail" /></td>
                    <td>{{ $pattern->probability }}</td>
                    <td><img src="{{ asset('pattern_image_files/' . $pattern->image_file) }}" width="80" class="img-thumbnail" /></td>
                    <td>{{ $pattern->fbx_file }}</td>
                    <td>
                        @if ($pattern->active)
                        <span class="label label-success">Active</span>
                        @else
                        <span class="label label-danger">Inactive</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <a title="Edit" class="btn btn-info" href="{{ URL::route('pattern.edit', $pattern->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                        <button title="Remove" class="btn btn-danger" id="deleteButton{{ $pattern->id }}" onclick="removePatternById({{ $pattern->id }})"><span class="glyphicon glyphicon-remove"></span></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="pull-left"><button class="btn btn-danger" id="remove_selected">Delete Selected</button></div>
        <div class="pull-right">
            {{ $patterns->links() }}
        </div>
    </div>
</div>
<script type="text/javascript">
    function removePatternById(id) {
        if (confirm('Do you really want to delete?')) {
            var url = '{{ URL::route('pattern.index') }}/' + id;
            $.ajax({
                url : url,
                type : 'DELETE',
                success : function(data) {
                    window.location.href = '{{ URL::route('pattern.index') }}';
                }
            });
        }
        return false;
    }
    $(function() {
        $("#checkall").click(function() {
            $("[id^=check_]").prop('checked', $("#checkall").is(':checked'));
        });
        $("#remove_selected").click(function() {
            if (confirm('Do you really want to delete?')) {
                var start = {{ ($patterns->getCurrentPage() - 1) * $patterns->getPerPage() + 1 }};
                var end = {{ $no }};
                for (var i = start; i <= end; i++) {
                    var checked = $("#check_" + i).is(':checked');
                    if (checked) {
                        var url = '{{ URL::route('pattern.index') }}/' + $("#pattern_id_" + i).val();
                        $.ajax({
                            url : url,
                            type : 'DELETE',
                            success : function(data) {
                                $("#tr_" + i).remove();
                            }
                        });
                    }
                }
                window.location.href = '{{ URL::route('pattern.index') }}';
            }
            return false;
        });
    });
</script>
@stop