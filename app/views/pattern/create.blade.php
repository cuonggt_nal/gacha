@extends('layouts.master')

@section('title')
GACHA | Add Pattern
@stop
@section('content')
{{ Form::open(array('route' => 'pattern.store', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true)) }}
<div class="panel panel-primary">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left" style="padding-top: 7.5px;">Add Pattern</h3>
        <div class="btn-group pull-right">
            <a class="btn btn-success" href="{{ URL::route('pattern.index') }}">List of Patterns</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-group @if ($errors->has('name')) has-error @endif">
            <label class="control-label col-xs-3">Pattern's name</label>
            <div class="col-xs-3">
                {{ Form::text('name', Input::old('name'), array('id' => 'name', 'class' => 'form-control')) }}
            </div>
            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
        </div>
        <div class="form-group @if ($errors->has('quantity')) has-error @endif">
            <label class="control-label col-xs-3">Quantity</label>
            <div class="col-xs-2">
                {{ Form::text('quantity', Input::old('quantity', 0), array('id' => 'quantity', 'class' => 'form-control')) }}
            </div>
            @if ($errors->has('quantity')) <p class="help-block">{{ $errors->first('quantity') }}</p> @endif
        </div>
        <div class="form-group @if ($errors->has('message_file')) has-error @endif">
            <label class="control-label col-xs-3">Message</label>
            <div class="col-xs-3">
                {{ Form::file('message_file', array('id' => 'message_file', 'class' => 'form-control')) }}
            </div>
            @if ($errors->has('message_file')) <p class="help-block">{{ $errors->first('message_file') }}</p> @endif
        </div>
        <div class="form-group @if ($errors->has('probability')) has-error @endif">
            <label class="control-label col-xs-3">Probability</label>
            <div class="col-xs-2">
                {{ Form::text('probability', Input::old('probability', 0), array('id' => 'probability', 'class' => 'form-control', 'maxlength' => 3)) }}
            </div>
            @if ($errors->has('probability')) <p class="help-block">{{ $errors->first('probability') }}</p> @endif
        </div>
        <div class="form-group @if ($errors->has('image_file')) has-error @endif">
            <label class="control-label col-xs-3">Image File</label>
            <div class="col-xs-3">
                {{ Form::file('image_file', array('id' => 'image_file', 'class' => 'form-control')) }}
            </div>
            @if ($errors->has('image_file')) <p class="help-block">{{ $errors->first('image_file') }}</p> @endif
        </div>
        <div class="form-group @if ($errors->has('fbx_file')) has-error @endif">
            <label class="control-label col-xs-3">3D File (FBX)</label>
            <div class="col-xs-3">
                {{ Form::file('fbx_file', array('id' => 'fbx_file', 'class' => 'form-control')) }}
            </div>
            @if ($errors->has('fbx_file')) <p class="help-block">{{ $errors->first('fbx_file') }}</p> @endif
        </div>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-9">
                <div class="checkbox">
                    <label>{{ Form::checkbox('active', true, Input::old('active', true), array('id' => 'active')) }} Active</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-9">
                {{ Form::button('Save', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
                {{ Form::button('Cancel', array('class' => 'btn btn-default', 'id' => 'cancelButton')) }}
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
<script type="text/javascript">
    $(function() {
        $("#cancelButton").click(function() {
            window.location = '{{ URL::route('pattern.index') }}';
        });
    });
</script>
@stop