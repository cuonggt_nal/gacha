<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ URL::route('home.index') }}">GACHA</a>
</div>
<div class="navbar-collapse collapse">
    @if (!empty($user))
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::route('pattern.index') }}">Patterns</a></li>
        <li><a href="{{ URL::route('result.index') }}">Player Results</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $user['username'] }}<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL::route('session.destroy') }}">Logout</a></li>
            </ul>
        </li>
    </ul>
    @endif
</div><!--/.navbar-collapse -->