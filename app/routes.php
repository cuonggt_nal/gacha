<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Authentication
 *
 * Allow a user to log in and log out of the application
 */
Route::get('login',     ['uses' => 'SessionController@create',  'as' => 'session.create']);
Route::post('login',     ['uses' => 'SessionController@store',  'as' => 'session.store']);
Route::get('logout',     ['uses' => 'SessionController@destroy',  'as' => 'session.destroy']);

Route::group(array('before' => 'auth'), function() {
    Route::get('/', ['uses' => 'HomeController@index',  'as' => 'home.index']);
    
    Route::resource('pattern', 'PatternController');
    Route::resource('result', 'ResultController');
});