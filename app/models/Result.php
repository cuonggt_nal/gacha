<?php

class Result extends Eloquent {

    use SoftDeletingTrait;
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'results';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

    public function pattern() {
        return $this->belongsTo('Pattern');
    }

}
